import requests
import json
from common.keys import PEXEL_API, WEATHER_API


def get_city_photo(city, state):
    url = f"https://api.pexels.com/v1/search?query={city},{state}"
    headers = {
        "Authorization": PEXEL_API,
    }
    r = requests.get(url, headers=headers)
    data = json.loads(r.content)
    print(data["photos"][0])
    print(data["photos"][0]["src"]["original"])
    return data["photos"][0]["src"][
        "original"
    ]  # look at the insomnia external api pull to see how the data is structured
    # print(r.content)  use to test weather the pull is working.  Run the create function and look in terminal


"https://api.openweathermap.org/data/2.5/weather?lat=25.7741728&lon=-80.19362&appid={WEATHER_API}&units=imperial"

def get_weather(city, state):
    coordinates_url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},USA&appid={WEATHER_API}"
    r = requests.get(coordinates_url)
    data = json.loads(r.content)
    lat = data[0]["lat"]
    lon = data[0]["lon"]
    weather_url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={WEATHER_API}&units=imperial"
    r2 = requests.get(weather_url)
    weather_data = json.loads(r2.content)
    weather_info = {
        "temperature": weather_data['main']['temp'],
        "description": weather_data['weather'][0]['description'],
        "humidity": weather_data['main']['humidity'],
    }
    return weather_info
