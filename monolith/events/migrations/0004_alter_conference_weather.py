# Generated by Django 4.0.3 on 2022-12-27 22:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0003_conference_weather'),
    ]

    operations = [
        migrations.AlterField(
            model_name='conference',
            name='weather',
            field=models.PositiveSmallIntegerField(default=''),
        ),
    ]
