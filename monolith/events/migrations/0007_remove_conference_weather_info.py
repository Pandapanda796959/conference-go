# Generated by Django 4.0.3 on 2022-12-27 23:24

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0006_remove_conference_weather_conference_weather_info'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='conference',
            name='weather_info',
        ),
    ]
